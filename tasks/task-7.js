/*
Завдання 7

Доповніть код так, щоб він коректно працював
 */

const array = ['value', () => 'showValue'];

const [value, showValue] = array;

console.log("Task #7", value, showValue());